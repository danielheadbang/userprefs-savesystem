﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Security.Cryptography;

public class SaveData
{
    #region File Values
    //default values
    static string defaultDirectoryPath = Application.persistentDataPath;
    //they can be changed at runtime
    /// <summary>
    /// The folder inside which the file will be saved
    /// </summary>
    static string directoryPath = string.Concat(defaultDirectoryPath, "/Saves");
    /// <summary>
    /// The name of the file
    /// </summary>
    static string filename = "Save";
    /// <summary>
    /// The extension of the file
    /// </summary>
    static string fileExtension = "sav";
    /// <summary>
    /// The final path of the file
    /// </summary>
    static string filePath = string.Format("{0}/{1}.{2}", directoryPath, filename, fileExtension);
    /// <summary>
    /// The base file name "Save"
    /// </summary>
    static readonly string baseFileName = filename;
#endregion

#region Events
    public delegate void SaveAction();
    public static event SaveAction OnSave;
    public static event SaveAction OnLoad;
    /// <summary>
    /// Are we using events when loading or saving?
    /// </summary>
    static bool useEvents = false;
#endregion

    /// <summary>
    /// Set the directory, file name, extension and if we are using events
    /// </summary>
    /// <param name="setDirectoryPath">The folder name path</param>
    /// <param name="setFileName">The name of the file</param>
    /// <param name="setFileExtension">The extension of the file</param>
    /// <param name="setUseEvents">Set to true if you are using events</param>
    public static void SetValues(string setDirectoryPath = null, string setFileName = null, string setFileExtension = null, bool setUseEvents = false)
    {
        directoryPath = setDirectoryPath ?? directoryPath;
        filename = setFileName ?? filename;
        fileExtension = setFileExtension ?? fileExtension;
        filePath = string.Format("{0}/{1}.{2}", directoryPath, filename, fileExtension);
        useEvents = setUseEvents;
    }

    /// <summary>
    /// Does the save file exist?
    /// </summary>
    /// <returns></returns>
    public static bool SaveFileExists()
    {
        if (File.Exists(filePath)) return true;
        else
        {
            #if UNITY_STANDALONE
            if (!Directory.Exists(directoryPath)) CreateDirectory();
            #endif
            return false;
        }
    }

    /// <summary>
    /// Save a file
    /// </summary>
    /// <param name="save">The object to be saved in the file</param>
    public static void Save(object save)
    {
        #if UNITY_STANDALONE
        if (!DirectoryExists()) CreateDirectory();
        #endif

        if (useEvents) OnSave();

        BinaryFormatter bf = new BinaryFormatter();
        using(FileStream file = File.Open(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite))
        {
            bf.Serialize(file, save);
        }
        //EncryptFile(filePath, filePath);

        //Debug.LogError("Saved " + save + " at " + filePath);
    }

    /// <summary>
    /// Return the byte array of a given object
    /// </summary>
    /// <param name="save"></param>
    /// <returns>the data as a byte array</returns>
    public static byte[] ToByteArray(object save)
    {
        byte[] data;
        BinaryFormatter bf = new BinaryFormatter();
        using (MemoryStream stream = new MemoryStream())
        {
            bf.Serialize(stream, save);
            data = stream.ToArray();
            return data;
        }
    }

    /// <summary>
    /// Return the value of a byte array as an object
    /// </summary>
    /// <param name="array"></param>
    /// <returns></returns>
    public static object FromByteArray(byte[] array)
    {
        BinaryFormatter bf = new BinaryFormatter();
        using (MemoryStream stream = new MemoryStream(array))
        {
            return bf.Deserialize(stream);
        }
    }

    /// <summary>
    /// Load a file
    /// </summary>
    /// <param name="fileToLoad">the object to be loaded</param>
    /// <returns>An object</returns>
    public static object Load(object fileToLoad)
    {
        #if UNITY_STANDALONE
        if (!DirectoryExists()) CreateDirectory();
        #endif

        if (useEvents) OnLoad();

        BinaryFormatter bf = new BinaryFormatter();
        using(FileStream file = File.Open(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite))
        {
            fileToLoad = bf.Deserialize(file);
        }

        //DecryptFile(filePath, filePath);

        //Debug.LogError("Loaded " + fileToLoad + " from " + filePath);

        return fileToLoad;
    }

    /// <summary>
    /// Deletes the save file
    /// </summary>
    public static void DeleteSaveFile()
    {
        if (File.Exists(filePath)) File.Delete(filePath);
        else return;
        Debug.Log("Deleted the file");
    }

    /// <summary>
    /// Get the name of the file
    /// </summary>
    /// <returns>The name of the file</returns>
    public static string GetFileName()
    {
        return filename;
    }

    /// <summary>
    /// Get the base file name
    /// </summary>
    /// <returns>"Save"</returns>
    public static string GetBaseFileName()
    {
        return baseFileName;
    }

    static bool DirectoryExists()
    {
        if (Directory.Exists(directoryPath)) return true;
        return false;
    }

#if UNITY_STANDALONE
    /// <summary>
    /// Create the save folder directory
    /// </summary>
    public static void CreateDirectory()
    {
        Directory.CreateDirectory(directoryPath);
    }
#endif

    /// <summary>
    /// Get the directory path
    /// </summary>
    /// <returns>The directory path</returns>
    public static string GetDirectoryPath()
    {
        return directoryPath;
    }

    /// <summary>
    /// Get the file with it's path
    /// </summary>
    /// <returns>The file with it's path</returns>
    public static string GetFilePath()
    {
        return filePath;
    }
}

