﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class DbData {
	#if UNITY_STANDALONE || UNITY_EDITOR
	public static string filesDirectoryPath = Application.dataPath + "/Files/";
	// public static string filesDirectoryPath = Application.streamingAssetsPath;
	#else
	// public static string filesDirectoryPath = "jar:file://" + Application.dataPath + "!/assets/";
	public static string filesDirectoryPath = Application.persistentDataPath;
	#endif

	public string fileName;
	public string filePath;
	public string fileExtension;

	public bool fileExists()
	{
		return File.Exists(filePath);
	}

	public DbData(string name, string extension)
	{
		fileExtension = extension;
		fileName = string.Format("{0}.{1}", name, fileExtension);
		filePath = Path.Combine(filesDirectoryPath, fileName);
	}
}
