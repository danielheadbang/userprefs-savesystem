﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Runtime.Serialization;
using System.Linq;

[Serializable]
public class DataContainer
{
    public Hashtable dataStored = new Hashtable();
}

public class UserPrefs
{
    /// <summary>
    /// Will we be saving every time user prefs is called?
    /// </summary>
    static bool _constantSaving = false;
    /// <summary>
    /// We will be restoring our values from playerprefs
    /// </summary>
    static bool restore = false;

    #region Events
    public delegate void UserPrefAction();
    public static event UserPrefAction OnReloadUserprefs;
    public static event UserPrefAction OnDeleteAll;
    public static event UserPrefAction OnSave;
    public static event UserPrefAction OnLoad;
    #endregion

    /// <summary>
    /// Initialize the userprefs
    /// </summary>
    /// <param name="constantSaving">True means saving the file on every call, false means only on Save()</param>
    /// <param name="restoreFromPP">do we check for playerprefs values before userprefs?</param>
    public static void Initialize(bool constantSaving = false, bool restoreFromPP = false)
    {
        _constantSaving = constantSaving;
#if UNITY_STANDALONE
        SaveData.Save(dataContainer ?? GetOrCreateDataContainer());
#endif
        restore = restoreFromPP;

        if(OnSave == null)
        {
            OnSave += HandleSave;
            OnLoad += HandleLoad;
            OnReloadUserprefs += HandleReload;
            OnDeleteAll += HandleDelete;
        }
        Debug.Log("Userprefs Initialized");
    }

    [RuntimeInitializeOnLoadMethod]
    static void Init()
    {
        Debug.Log("Initializing userprefs...");
        Initialize();
    }

#if UNITY_STANDALONE
    static DataContainer dataContainer = GetOrCreateDataContainer();
#else
    static DataContainer dataContainer = new DataContainer();
#endif

#if UNITY_STANDALONE
    static DataContainer GetOrCreateDataContainer()
    {
        DataContainer dc = new DataContainer();
        try
        {
            dc = SaveData.Load(dataContainer) as DataContainer ?? new DataContainer();
            return dc;
        }
        catch (SerializationException)
        {
            Debug.LogError("Data might be corrupted, rebuilding...");
            SaveData.Save(dc);
            dc = SaveData.Load(dataContainer) as DataContainer ?? new DataContainer();
            return dc;
        }
        catch (DirectoryNotFoundException)
        {
            Debug.LogError("Save folder does not exist, creating...");
            SaveData.CreateDirectory();
            SaveData.Save(dc);
            dc = SaveData.Load(dataContainer) as DataContainer ?? new DataContainer();
            return dc;
        }
        catch (System.IO.IsolatedStorage.IsolatedStorageException)
        {
            Debug.LogError("Isolated storage error...");
            SaveData.CreateDirectory();
            SaveData.Save(dc);
            dc = SaveData.Load(dataContainer) as DataContainer ?? new DataContainer();
            return dc;
        }
    }
#endif

    public static void SetString(string key, string value)
    {
        SaveContainer(key, value);
    }

    public static string GetString(string key, string defaultValue = "")
    {
        if (restore) RestoreValue(key, typeof(string));

        LoadContainer();

        if (dataContainer.dataStored.ContainsKey(key))
        {
            return dataContainer.dataStored[key].ToString();
        }
        return defaultValue;
    }

    public static void SetInt(string key, int value)
    {
        SaveContainer(key, value);
    }

    public static int GetInt(string key, int defaultValue = 0)
    {
        if(restore) RestoreValue(key, typeof(int));

        LoadContainer();

        if (dataContainer.dataStored.ContainsKey(key))
        {
            return (int)dataContainer.dataStored[key];
        }
        return defaultValue;
    }

    public static void SetFloat(string key, float value)
    {
        SaveContainer(key, value);
    }

    public static float GetFloat(string key, float defaultValue = 0)
    {
        if (restore) RestoreValue(key, typeof(float));

        LoadContainer();

        if (dataContainer.dataStored.ContainsKey(key))
        {
            return (float)dataContainer.dataStored[key];
        }
        return defaultValue;
    }

    public static void SetBool(string key, bool value)
    {
        SaveContainer(key, value);
    }

    public static bool GetBool(string key, bool defaultValue = false)
    {
        if (restore) RestoreValue(key, typeof(bool));

        LoadContainer();

        if (dataContainer.dataStored.ContainsKey(key))
        {
            return (bool)dataContainer.dataStored[key];
        }
        return defaultValue;
    }

    public static void SetObject(string key, object value)
    {
        SaveContainer(key, value);
    }

    public static object GetObject(string key, object defaultValue = null)
    {
        LoadContainer();

        if (dataContainer.dataStored.ContainsKey(key))
        {
            return dataContainer.dataStored[key];
        }
        return defaultValue;
    }

#region Other methods

    ///<summary> does the database contain the key? </summary>
    public static bool HasKey(string name)
    {
        if (dataContainer.dataStored.ContainsKey(name)) return true;
        return false;
    }

    ///<summary> delete the set key from the database </summary>
    public static void DeleteKey(string key)
    {
        if (dataContainer.dataStored.ContainsKey(key))
            dataContainer.dataStored.Remove(key);
    }

    ///<summary> delete all stored values on the database </summary>
    public static void DeleteAll()
    {
        OnDeleteAll();
        dataContainer.dataStored.Clear();
        Save();
    }

    /// <summary>
    /// Reloads the UserPrefs
    /// </summary>
    public static void ReloadUserData()
    {
        OnReloadUserprefs();
        dataContainer = new DataContainer();
        LoadContainer();
    }

    /// <summary>
    /// Restore values from previous playerprefs backups
    /// </summary>
    /// <param name="key"></param>
    /// <param name="variableType"></param>
    static void RestoreValue(string key, Type variableType)
    {
        if (PlayerPrefs.HasKey(key))
        {
            Debug.Log(key + " value was found, restoring...");

            if (variableType == typeof(int)) SetInt(key, PlayerPrefs.GetInt(key));
            else if (variableType == typeof(float)) SetFloat(key, PlayerPrefs.GetFloat(key));
            else if (variableType == typeof(string)) SetString(key, PlayerPrefs.GetString(key));
            else if (variableType == typeof(bool)) SetBool(key, PlayerPrefs.GetInt(key) == 1);

            PlayerPrefs.DeleteKey(key);
        }
    }

    /// <summary>
    /// Return the current data as a byte array
    /// </summary>
    /// <returns></returns>
    public static byte[] dataAsByteArray
    {
        get
        {
            return DataContainerAsByteArray(dataContainer);
        }
    }

    static byte[] DataContainerAsByteArray(DataContainer container){
        byte[] data;
        System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
        using (MemoryStream stream = new MemoryStream())
        {
            bf.Serialize(stream, container);
            data = stream.ToArray();
            return data;
        }
    }

    /// <summary>
    /// Set the datacontainer to restore the values from a byte array
    /// </summary>
    /// <param name="array"></param>
    public static void RestoreFromByteArray(byte[] array)
    {
        dataContainer = FromByteArray(array);
    }

    static DataContainer FromByteArray(byte[] array)
    {
        System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
        using (MemoryStream stream = new MemoryStream(array))
        {
            return bf.Deserialize(stream) as DataContainer;
        }
    }

    public static string[] GetAllKeysStored()
    {
        return dataContainer.dataStored.Keys.Cast<string>().ToArray();
        return null;
    }
#endregion

#region Saving and Loading the DataContainer
    static void SaveContainer(string key, object value)
    {
        if (dataContainer.dataStored.ContainsKey(key))
            dataContainer.dataStored[key] = value;
        else
            dataContainer.dataStored.Add(key, value);

        if(_constantSaving) Save();
    }

    public static void Save()
    {
        OnSave();
    }

    static void LoadContainer()
    {
        OnLoad();
    }
    #endregion

    #region EventHandlers
    static void HandleSave()
    {
#if UNITY_STANDALONE
        SaveData.Save(dataContainer);
#endif
    }

    static void HandleReload()
    {

    }

    static void HandleLoad()
    {
#if UNITY_STANDALONE
        dataContainer = SaveData.Load(dataContainer) as DataContainer;
#endif
    }

    static void HandleDelete()
    {

    }
    #endregion
}
